
// Hàm hiển thị tên
function getName(name){
	alert(name);
}


//Khởi tạo component Khoapham
class Khoapham extends React.Component{


	constructor(props){
		super(props);
    //Khởi tạo state totalStudents
    this.state = {
    	totalStudents : 10,
    };
     //
     this.addStudent = this.addStudent.bind(this);
 }


  	//Thêm học viên
  	addStudent(){
  		this.setState({totalStudents: this.state.totalStudents + 1});
  	}


	// Hiển thị ra màn hình
	render(){
		return(
			<div>

				<h1 className="mauvang"> Toi ten la {this.props.ten} tuoi: {this.props.tuoi} </h1>

				<p>So hoc vien: {this.state.totalStudents}</p>

				<button onClick={()=>{getName(this.props.ten)}} >thong tin</button>

				<button onClick={this.addStudent} >Add student</button>

				<Khoahoc />

			</div>
			);
		}


	}

	//Khởi tại component Khoahoc
	class Khoahoc extends React.Component{
		render(){
			return <h3> Lap trinh ReactJS </h3>;
		}
	}

	ReactDOM.render(
		<div>
			<Khoapham ten="DangKhoa" tuoi="21"> day la props children </Khoapham>
			<Khoapham ten="ThuyLinh" tuoi="18"> </Khoapham>
		</div>
	,document.getElementById('root'));
